package com.training.northwind.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShipperTest {

    private static final String testCompanyName = "Junit Test Company";
    private static final String testPhoneNumber = "1234";
    private Shipper shipper;

    @BeforeEach
    public void setup() {
        this.shipper = new Shipper();
    }

    @Test
    public void setPhoneTest() {

        this.shipper.setPhone(testPhoneNumber);
        String phone = shipper.getPhone();

        assertEquals(testPhoneNumber, phone);

    }

    @Test
    public void setCompanyNameTest() {

        this.shipper.setCompanyName(testCompanyName);
        String name = shipper.getCompanyName();

        assertEquals(testCompanyName, name);

    }
}
