package com.training.northwind.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ShipperControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shipperControllerFindAllSuccess() throws Exception {
        this.mockMvc.perform(get("/api/v1/shipper"))
                            .andDo(print())
                            .andExpect(status().isOk())
                            .andReturn();
    }

    @Test
    public void shipperControllerNotFound() throws Exception {
        this.mockMvc.perform(get("/api/v1/ships"))
                            .andDo(print())
                            .andExpect(status().is(404))
                            .andReturn();
    }

}
