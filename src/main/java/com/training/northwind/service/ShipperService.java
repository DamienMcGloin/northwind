package com.training.northwind.service;

import com.training.northwind.entities.Shipper;
import com.training.northwind.repository.ShipperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShipperService {

    @Autowired
    ShipperRepository shipperRepository;

    public List<Shipper> findAll() {
        return shipperRepository.findAll();
    }

    public Shipper findById(long id) {
        return shipperRepository.findById(id).get();
    }

    /*
    public List<Shipper> findByPhone(String phone) {
        return shipperRepository.findShipperByPhone("1234")
    }

     */

    public Shipper save(Shipper shipper) {
        return shipperRepository.save(shipper);
    }

}
